"use strict"

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
let gProductLineId = 0;

let table = $('#product-line-table').DataTable({
    columns: [
        { 
            data: 'id',
            width: '5%'
        
        },
        { 
            data: 'productLine',
            width: '15%'
        },
        { 
            data: 'description',
            width: '80%'
        },
        { data: 'Action' }
    ],
    columnDefs: [
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-warning" style="cursor:pointer;"></i>
            || <i class="fas fa-trash text-danger" style="cursor:pointer;"></i>`,
        }
    ],
});

let _productLine = {
    newData: {
        productLine: '',
        description: ''
    },
    // CREATE - Tạo lớp mới
    onCreateNewClick() {
        gProductLineId = 0;
        this.newData = {
            productLine: $('#inp-product-line').val().trim(),
            description: $('#inp-desc').val().trim(),
        }
        if (validate(this.newData)) {
            $.ajax({
                url: `http://localhost:8080/product-lines`,
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(this.newData),
                success: () => {
                    alert('successfully create new Product Line');
                    $.get(`http://localhost:8080/product-lines`, loadDataToTable);
                    resetDataInput();
                },
                error: (err) => alert(err.responseText),
            });
        }
    },
    onUpdateModalClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = table.row(vSelectedRow).data();
        gProductLineId = vSelectedData.id;

        $('#modal-update-product-line').modal('show');
        $.get(`http://localhost:8080/product-lines/${gProductLineId}`, loadDataToInput);

    },
    onConfirmUpdateClick() {
        this.newData = {
            productLine: $('#modal-inp-product-line').val().trim(),
            description: $('#modal-inp-desc').val().trim()
        };
        if (validate(this.newData)) {
            $.ajax({
                url: `http://localhost:8080/product-lines/${gProductLineId}`,
                method: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify(this.newData),
                success: () => {
                    alert('successfully update Product Line with id: ' + gProductLineId);
                    $('#modal-update-product-line').modal('hide');
                    $.get(`http://localhost:8080/product-lines`, loadDataToTable);
                },
                error: (err) => alert(err.responseText),
            });
        }

    },
    onDeleteClick() {
        $('#modal-delete-product-line').modal('show');
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = table.row(vSelectedRow).data();
        gProductLineId = vSelectedData.id;

    },
    onConfirmDeleteClick() {
        $.ajax({
            url: `http://localhost:8080/product-lines/${gProductLineId}`,
            method: 'delete',
            success: () => {
                alert('successfully delete Product Line with id: ' + gProductLineId);
                $('#modal-delete-product-line').modal('hide');
                $.get(`http://localhost:8080/product-lines`, loadDataToTable);
            },
            error: (err) => alert(err.responseText),
        });
    },
};

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$.get(`http://localhost:8080/product-lines`, loadDataToTable);

$('#btn-create-product-line').click(_productLine.onCreateNewClick);
$('#btn-update-product-line').click(_productLine.onConfirmUpdateClick);
$('#btn-confirm-delete-product-line').click(_productLine.onConfirmDeleteClick);

$('#product-line-table').on('click', '.fa-edit', _productLine.onUpdateModalClick);
$('#product-line-table').on('click', '.fa-trash', _productLine.onDeleteClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */

function loadDataToTable(paramData) {
    table.clear();
    table.rows.add(paramData);
    table.draw();
}
function loadDataToInput(paramData) {
    $('#modal-inp-product-line').val(paramData.productLine)
    $('#modal-inp-desc').val(paramData.description)
}
function validate(paramData) {
    let vResult = true;
    try {
        if (paramData.productLine == '') {
            vResult = false;
            throw `Product Line can't be empty`;
        }

        if (paramData.description == '') {
            vResult = false;
            throw `description can't be empty`;
        }
    } catch (e) {
        alert(e);
    }
    return vResult;
}

function resetDataInput() {
    $('#inp-product-line').val('');
    $('#inp-desc').val('');
}


