"use strict"

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
let gId = 0;

let table = $('#table').DataTable({
    columns: [
        { data: 'id'},
        { data: 'city'},
        { data: 'phone'},
        { data: 'addressLine'},
        { data: 'state'},
        { data: 'country'},
        { data: 'territory'},
        { data: 'Action' }
    ],
    columnDefs: [
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-warning" style="cursor:pointer;"></i>
            || <i class="fas fa-trash text-danger" style="cursor:pointer;"></i>`,
        }
    ],
});

let _office = {
    newData: {
        city: '',
        phone: '',
        addressLine: '',
        state: '',
        country: '',
        territory: '',
    },
    // CREATE - Tạo lớp mới
    onCreateNewClick() {
        gId = 0;
        this.newData = {
            city: $('#inp-city').val().trim(),
            phone: $('#inp-phone').val().trim(),
            addressLine: $('#inp-address').val().trim(),
            state: $('#inp-state').val().trim(),
            country: $('#inp-country').val(),
            territory: $('#inp-territory').val(),
        }
        if (validate(this.newData)) {
            $.ajax({
                url: `http://localhost:8080/offices`,
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(this.newData),
                success: () => {
                    alert('successfully create new Office');
                    $.get(`http://localhost:8080/offices`, loadDataToTable);
                    resetDataInput();
                },
                error: (err) => alert(err.responseText),
            });
        }
    },
    onUpdateModalClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = table.row(vSelectedRow).data();
        gId = vSelectedData.id;

        $('#modal-update').modal('show');
        $.get(`http://localhost:8080/offices/${gId}`, loadDataToInput);

    },
    onConfirmUpdateClick() {
        this.newData = {
            city: $('#modal-inp-city').val().trim(),
            phone: $('#modal-inp-phone').val().trim(),
            addressLine: $('#modal-inp-address').val().trim(),
            state: $('#modal-inp-state').val().trim(),
            country: $('#modal-inp-country').val(),
            territory: $('#modal-inp-territory').val(),
        };
        if (validate(this.newData)) {
            $.ajax({
                url: `http://localhost:8080/offices/${gId}`,
                method: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify(this.newData),
                success: () => {
                    alert('successfully update Office with id: ' + gId);
                    $('#modal-update').modal('hide');
                    $.get(`http://localhost:8080/offices`, loadDataToTable);
                },
                error: (err) => alert(err.responseText),
            });
        }

    },
    onDeleteClick() {
        $('#modal-delete').modal('show');
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = table.row(vSelectedRow).data();
        gId = vSelectedData.id;

    },
    onConfirmDeleteClick() {
        $.ajax({
            url: `http://localhost:8080/offices/${gId}`,
            method: 'delete',
            success: () => {
                alert('successfully delete Office with id: ' + gId);
                $('#modal-delete').modal('hide');
                $.get(`http://localhost:8080/offices`, loadDataToTable);
            },
            error: (err) => alert(err.responseText),
        });
    },
};

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$.get(`http://localhost:8080/offices`, loadDataToTable);

$('#btn-create').click(_office.onCreateNewClick);
$('#btn-update').click(_office.onConfirmUpdateClick);
$('#btn-confirm-delete').click(_office.onConfirmDeleteClick);

$('#table').on('click', '.fa-edit', _office.onUpdateModalClick);
$('#table').on('click', '.fa-trash', _office.onDeleteClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */

function loadDataToTable(paramData) {
    table.clear();
    table.rows.add(paramData);
    table.draw();
}
function loadDataToInput(paramData) {
    $('#modal-inp-city').val(paramData.city)
    $('#modal-inp-phone').val(paramData.phone)
    $('#modal-inp-address').val(paramData.addressLine)
    $('#modal-inp-state').val(paramData.state)
    $('#modal-inp-country').val(paramData.country)
    $('#modal-inp-territory').val(paramData.territory)
}
function validate(paramData) {
    let vResult = true;
    try {
        if (paramData.city == '') {
            vResult = false;
            throw `city can't be empty`;
        }

        if (paramData.phone == '') {
            vResult = false;
            throw `phone can't be empty`;
        }
        if (paramData.address == '') {
            vResult = false;
            throw `address can't be empty`;
        }

        if (paramData.country == '') {
            vResult = false;
            throw `country can't be empty`;
        }
        
    } catch (e) {
        alert(e);
    }
    return vResult;
}

function resetDataInput() {
    $('#inp-city').val('')
    $('#inp-phone').val('')
    $('#inp-address').val('')
    $('#inp-state').val('')
    $('#inp-country').val('')
    $('#inp-territory').val('')
}


