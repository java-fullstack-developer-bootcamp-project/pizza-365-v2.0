"use strict"

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
let gCustomerId = 0;
let gPaymentId = 0;
let gSelectCustomer = $('#select-customer')

let paymentTable = $('#payment-table').DataTable({
    columns: [
        { data: 'id' },
        { data: 'customerFullName' },
        { data: 'checkNumber' },
        {
            data: 'paymentDate',
            render: function (data, type, row) {
                return moment(row.paymentDate).format('DD/MM/YYYY HH:mm:ss a');
            }
        },
        { data: 'ammount' },
        { data: 'Action' }
    ],
    columnDefs: [
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-warning" style="cursor:pointer;"></i>
            || <i class="fas fa-trash text-danger" style="cursor:pointer;"></i>`,
        }
    ],
});

let payment = {
    newPayment: {
        checkNumber: '',
        paymentDate: '',
        ammount: '',
    },
    onNewPaymentClick() {
        gPaymentId = 0;
        this.newPayment = {
            checkNumber: $('#inp-check-number').val().trim(),
            paymentDate: $('#inp-payment-date').val().trim(),
            ammount: $('#inp-amount').val()
        }
        if (validatePayment(this.newPayment)) {
            if (gCustomerId == 0) {
                alert(`Please select a customer to create new payment!`);
            } else {
                $.ajax({
                    url: `http://localhost:8080/customers/${gCustomerId}/payments`,
                    method: 'POST',
                    data: JSON.stringify(this.newPayment),
                    contentType: 'application/json',
                    success: () => {
                        alert(`Payment created successfully`);
                        $.get(
                            `http://localhost:8080/customers/${gCustomerId}/payments`,
                            loadPaymentToTable,
                        );
                        resetPaymentInput();
                    },
                });
            }
        }
    },
    onUpdateModalClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = paymentTable.row(vSelectedRow).data();
        gPaymentId = vSelectedData.id;
        $('#modal-update-payment').modal('show');
        $.get(
            `http://localhost:8080/payments/${gPaymentId}`,
            loadPaymentToInput
        );
    },
    onConfirmUpdatePaymentClick() {
        this.newPayment = {
            checkNumber: $('#modal-inp-check-number').val().trim(),
            paymentDate: $('#modal-inp-payment-date').val().trim(),
            ammount: $('#modal-inp-amount').val().trim()
        };
        if (validatePayment(this.newPayment)) {
            $.ajax({
                url: `http://localhost:8080/payments/${gPaymentId}`,
                method: 'PUT',
                data: JSON.stringify(this.newPayment),
                contentType: 'application/json',
                success: () => {
                    alert(`Payment updated successfully`);
                    $('#modal-update-payment').modal('hide');
                    $.get(`http://localhost:8080/payments`, loadPaymentToTable);
                },
                error: (err) => alert(err.responseText)
            });
        }
    },
    onDeletePaymentByIdClick() {
        $('#modal-delete-payment').modal('show');
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = paymentTable.row(vSelectedRow).data();
        gPaymentId = vSelectedData.id;
    },
    oConfirmDeletePaymentClick() {
        $.ajax({
            url: `http://localhost:8080/payments/${gPaymentId}`,
            method: 'delete',
            success: () => {
                alert(`Payment with id ${gPaymentId} was successfully deleted`);
                $('#modal-delete-payment').modal('hide');
                $.get(`http://localhost:8080/payments`, loadPaymentToTable);

            },
            error: (err) => alert(err.responseText),
        });
    },
};
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$.get(`http://localhost:8080/customers`, loadCustomerToSelect);
onPageLoading();


gSelectCustomer.change(onGetCustomerChange);

$('#btn-create-payment').click(payment.onNewPaymentClick);
$('#btn-update-payment').click(payment.onConfirmUpdatePaymentClick);
$('#btn-confirm-delete-payment').click(payment.oConfirmDeletePaymentClick);

$('#payment-table').on('click', '.fa-edit', payment.onUpdateModalClick);
$('#payment-table').on('click', '.fa-trash', payment.onDeletePaymentByIdClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Chọn lớp thì load học sinh ra bảng
function onGetCustomerChange(event) {
    gCustomerId = event.target.value;
    if (gCustomerId == 0) {
        $.get(`http://localhost:8080/payments`, loadPaymentToTable);
    } else {
        $.get(
            `http://localhost:8080/customers/${gCustomerId}/payments`,
            loadPaymentToTable,
        );
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
function loadCustomerToSelect(paramCustomer) {
    paramCustomer.forEach((Customer) => {
        $('<option>', {
            text: Customer.id,
            value: Customer.id,
        }).appendTo(gSelectCustomer);
    });
}
function loadPaymentToTable(paramPayment) {
    paymentTable.clear();
    paymentTable.rows.add(paramPayment);
    paymentTable.draw();
}

function validatePayment(paramPayment) {
    let vResult = true;
    try {
        if (paramPayment.checkNumber == '') {
            vResult = false;
            throw `Check number can't empty`;
        }
        if (paramPayment.paymentDate == '') {
            vResult = false;
            throw `Payment date can't empty`;
        }

        if (paramPayment.ammount == '') {
            vResult = false;
            throw `Amount can't empty`;
        }
    } catch (e) {
        alert(e);
    }
    return vResult;
}
function resetPaymentInput() {
    $('#inp-check-number').val('');
    $('#inp-payment-date').val('');
    $('#inp-amount').val('');
}
function loadPaymentToInput(paramPayment) {
    $('#modal-inp-check-number').val(paramPayment.checkNumber);
    $('#modal-inp-payment-date').val(moment(new Date(paramPayment.paymentDate)).format('YYYY-MM-DDTHH:mm'));
    $('#modal-inp-amount').val(paramPayment.ammount);
   
}

function onPageLoading() {
    let urlString = window.location.href;
    let url = new URL(urlString);
    gCustomerId = url.searchParams.get('customerId');
    console.log(gCustomerId)
    if (urlString.includes('?')) {
        $.get(`http://localhost:8080/customers`, function() {
            gSelectCustomer.val(gCustomerId);
        });
        $.get(`http://localhost:8080/customers/${gCustomerId}/payments`, loadPaymentToTable);
    }
    else {
        $.get(`http://localhost:8080/payments`, loadPaymentToTable);
    }
}
