"use strict"

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
let gProductLineId = 0;
let gProductId = 0;
let gSelectProductLine = $('#select-product-line');

let productTable = $('#product-table').DataTable({
    columns: [
        { data: 'id' },
        { 
            data: 'productLine.id',
            width: '2%',
        },
        { data: 'code' },
        { 
            data: 'name',
            width: '25%',
        },
        { data: 'scale' },
        { 
            data: 'vendor',
            width: '15%',
        
        },
        { data: 'quantityInStock' },
        { data: 'buyPrice' },
        { data: 'Action' }
    ],
    columnDefs: [
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-warning" style="cursor:pointer;"></i>
            || <i class="fas fa-trash text-danger" style="cursor:pointer;"></i>`,
        }
    ],
});

let product = {
    newProduct: {
        code: '',
        name: '',
        description: '',
        scale: '',
        vendor: '',
        quantityInStock: '',
        buyPrice: ''
    },
    onNewProductClick() {
        gProductId = 0;
        this.newProduct = {
            code: $('#inp-product-code').val().trim(),
            name: $('#inp-product-name').val().trim(),
            description: $('#inp-desc').val().trim(),
            scale: $('#inp-scale').val().trim(),
            vendor: $('#inp-vendor').val().trim(),
            quantityInStock: $('#inp-quantity').val().trim(),
            buyPrice: $('#inp-price').val().trim(),
        }
        if (validateProduct(this.newProduct)) {
            if (gProductLineId == 0) {
                alert(`Please select a product line to create new product!`);
            } else {
                $.ajax({
                    url: `http://localhost:8080/product-lines/${gProductLineId}/products`,
                    method: 'POST',
                    data: JSON.stringify(this.newProduct),
                    contentType: 'application/json',
                    success: () => {
                        alert(`Product created successfully`);
                        $.get(
                            `http://localhost:8080/product-lines/${gProductLineId}/products`,
                            loadProductToTable,
                        );
                        resetProductInput();
                    },
                });
            }
        }
    },
    onUpdateModalClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = productTable.row(vSelectedRow).data();
        gProductId = vSelectedData.id;
        $('#modal-update-product').modal('show');
        $.get(
            `http://localhost:8080/products/${gProductId}`,
            loadProductToInput
        );
    },
    onConfirmUpdateProductClick() {
        this.newProduct = {
            code: $('#modal-inp-product-code').val().trim(),
            name: $('#modal-inp-product-name').val().trim(),
            description: $('#modal-inp-desc').val().trim(),
            scale: $('#modal-inp-scale').val().trim(),
            vendor: $('#modal-inp-vendor').val().trim(),
            quantityInStock: $('#modal-inp-quantity').val().trim(),
            buyPrice: $('#modal-inp-price').val().trim()
        };
        if (validateProduct(this.newProduct)) {
            $.ajax({
                url: `http://localhost:8080/products/${gProductId}`,
                method: 'PUT',
                data: JSON.stringify(this.newProduct),
                contentType: 'application/json',
                success: () => {
                    alert(`Product updated successfully`);
                    $('#modal-update-product').modal('hide');
                    $.get(`http://localhost:8080/products`, loadProductToTable);
                },
                error: (err) => alert(err.responseText)
            });
        }
    },
    onDeleteProductByIdClick() {
        $('#modal-delete-product').modal('show');
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = productTable.row(vSelectedRow).data();
        gProductId = vSelectedData.id;
    },
    onConfirmDeleteProductClick() {
        $.ajax({
            url: `http://localhost:8080/products/${gProductId}`,
            method: 'delete',
            success: () => {
                alert(`Product with id ${gProductId} was successfully deleted`);
                $('#modal-delete-product').modal('hide');
                $.get(`http://localhost:8080/products`, loadProductToTable);

            },
            error: (err) => alert(err.responseText),
        });
    },
};
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$.get(`http://localhost:8080/product-lines`, loadProductLineToSelect);
$.get(`http://localhost:8080/products`, loadProductToTable);

gSelectProductLine.change(onGetCustomerChange);

$('#btn-create-product').click(product.onNewProductClick);
$('#btn-update-product').click(product.onConfirmUpdateProductClick);
$('#btn-confirm-delete-product').click(product.onConfirmDeleteProductClick);

$('#product-table').on('click', '.fa-edit', product.onUpdateModalClick);
$('#product-table').on('click', '.fa-trash', product.onDeleteProductByIdClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Chọn lớp thì load học sinh ra bảng
function onGetCustomerChange(event) {
    gProductLineId = event.target.value;
    if (gProductLineId == 0) {
        $.get(`http://localhost:8080/products`, loadProductToTable);
    } else {
        $.get(
            `http://localhost:8080/product-lines/${gProductLineId}/products`,
            loadProductToTable,
        );
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
function loadProductLineToSelect(paramProductLine) {
    paramProductLine.forEach((ProductLine) => {
        $('<option>', {
            text: ProductLine.productLine,
            value: ProductLine.id,
        }).appendTo(gSelectProductLine);
    });
}
function loadProductToTable(paramProduct) {
    productTable.clear();
    productTable.rows.add(paramProduct);
    productTable.draw();
}

function validateProduct(paramProduct) {
    let vResult = true;
    try {
        if (paramProduct.code == '') {
            vResult = false;
            throw `Code can't empty`;
        }
        if (paramProduct.name == '') {
            vResult = false;
            throw `Name can't empty`;
        }

        if (paramProduct.scale == '') {
            vResult = false;
            throw `Scale can't empty`;
        }
        if (paramProduct.vendor == '') {
            vResult = false;
            throw `Vendor can't empty`;
        }
        if (paramProduct.quantityInStock == '') {
            vResult = false;
            throw `Quantity can't empty`;
        }

        if (paramProduct.buyPrice == '') {
            vResult = false;
            throw `Price can't empty`;
        }
    } catch (e) {
        alert(e);
    }
    return vResult;
}
function resetProductInput() {
    $('#inp-product-code').val('')
    $('#inp-product-name').val('')
    $('#inp-desc').val('')
    $('#inp-scale').val('')
    $('#inp-vendor').val('')
    $('#inp-quantity').val('')
    $('#inp-price').val('')
}
function loadProductToInput(paramProduct) {
    $('#modal-inp-product-code').val(paramProduct.code)
    $('#modal-inp-product-name').val(paramProduct.name)
    $('#modal-inp-desc').val(paramProduct.description)
    $('#modal-inp-scale').val(paramProduct.scale)
    $('#modal-inp-vendor').val(paramProduct.vendor)
    $('#modal-inp-quantity').val(paramProduct.quantityInStock)
    $('#modal-inp-price').val(paramProduct.buyPrice)
   
}

