"use strict"

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
let gId = 0;

let table = $('#table').DataTable({
    columns: [
        { data: 'id'},
        { data: 'firstName'},
        { data: 'lastName'},
        { data: 'extension'},
        { data: 'email'},
        { data: 'officeCode'},
        { data: 'reportTo'},
        { data: 'jobTitle'},
        { data: 'Action' }
    ],
    columnDefs: [
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-warning" style="cursor:pointer;"></i>
            || <i class="fas fa-trash text-danger" style="cursor:pointer;"></i>`,
        }
    ],
});

let _employee = {
    newData: {
        firstName: '',
        lastName: '',
        extension: '',
        email: '',
        officeCode: '',
        reportTo: '',
        jobTitle: '',
    },
    // CREATE - Tạo lớp mới
    onCreateNewClick() {
        gId = 0;
        this.newData = {
            firstName: $('#inp-first-name').val().trim(),
            lastName: $('#inp-last-name').val().trim(),
            extension: $('#inp-extension').val().trim(),
            email: $('#inp-email').val().trim(),
            officeCode: $('#inp-office-code').val(),
            reportTo: $('#inp-report-to').val(),
            jobTitle: $('#inp-job-title').val().trim()
        }
        if (validate(this.newData)) {
            $.ajax({
                url: `http://localhost:8080/employees`,
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(this.newData),
                success: () => {
                    alert('successfully create new Employee');
                    $.get(`http://localhost:8080/employees`, loadDataToTable);
                    resetDataInput();
                },
                error: (err) => alert(err.responseText),
            });
        }
    },
    onUpdateModalClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = table.row(vSelectedRow).data();
        gId = vSelectedData.id;

        $('#modal-update').modal('show');
        $.get(`http://localhost:8080/employees/${gId}`, loadDataToInput);

    },
    onConfirmUpdateClick() {
        this.newData = {
            firstName: $('#modal-inp-first-name').val().trim(),
            lastName: $('#modal-inp-last-name').val().trim(),
            extension: $('#modal-inp-extension').val().trim(),
            email: $('#modal-inp-email').val().trim(),
            officeCode: $('#modal-inp-office-code').val(),
            reportTo: $('#modal-inp-report-to').val(),
            jobTitle: $('#modal-inp-job-title').val().trim()
        };
        if (validate(this.newData)) {
            $.ajax({
                url: `http://localhost:8080/employees/${gId}`,
                method: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify(this.newData),
                success: () => {
                    alert('successfully update Employee with id: ' + gId);
                    $('#modal-update').modal('hide');
                    $.get(`http://localhost:8080/employees`, loadDataToTable);
                },
                error: (err) => alert(err.responseText),
            });
        }

    },
    onDeleteClick() {
        $('#modal-delete').modal('show');
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = table.row(vSelectedRow).data();
        gId = vSelectedData.id;

    },
    onConfirmDeleteClick() {
        $.ajax({
            url: `http://localhost:8080/employees/${gId}`,
            method: 'delete',
            success: () => {
                alert('successfully delete Employee with id: ' + gId);
                $('#modal-delete').modal('hide');
                $.get(`http://localhost:8080/employees`, loadDataToTable);
            },
            error: (err) => alert(err.responseText),
        });
    },
};

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$.get(`http://localhost:8080/employees`, loadDataToTable);

$('#btn-create').click(_employee.onCreateNewClick);
$('#btn-update').click(_employee.onConfirmUpdateClick);
$('#btn-confirm-delete').click(_employee.onConfirmDeleteClick);

$('#table').on('click', '.fa-edit', _employee.onUpdateModalClick);
$('#table').on('click', '.fa-trash', _employee.onDeleteClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */

function loadDataToTable(paramData) {
    table.clear();
    table.rows.add(paramData);
    table.draw();
}
function loadDataToInput(paramData) {
    $('#modal-inp-first-name').val(paramData.firstName)
    $('#modal-inp-last-name').val(paramData.lastName)
    $('#modal-inp-extension').val(paramData.extension)
    $('#modal-inp-email').val(paramData.email)
    $('#modal-inp-office-code').val(paramData.officeCode)
    $('#modal-inp-report-to').val(paramData.reportTo)
    $('#modal-inp-job-title').val(paramData.jobTitle)
}
function validate(paramData) {
    let vResult = true;
    try {
        if (paramData.firstName == '') {
            vResult = false;
            throw `First name can't be empty`;
        }

        if (paramData.lastName == '') {
            vResult = false;
            throw `Last name can't be empty`;
        }
        if (paramData.extension == '') {
            vResult = false;
            throw `Extension can't be empty`;
        }

        if (paramData.email == '') {
            vResult = false;
            throw `email can't be empty`;
        }
        if (paramData.officeCode == '') {
            vResult = false;
            throw `Office code can't be empty`;
        }

        if (paramData.jobTitle == '') {
            vResult = false;
            throw `Job title can't be empty`;
        }
    } catch (e) {
        alert(e);
    }
    return vResult;
}

function resetDataInput() {
    $('#inp-first-name').val('')
    $('#inp-last-name').val('')
    $('#inp-extension').val('')
    $('#inp-email').val('')
    $('#inp-office-code').val('')
    $('#inp-report-to').val('')
    $('#inp-job-title').val('')
}


