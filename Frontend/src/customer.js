"use strict"

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
let gCustomerId = 0;

var usaDB = [];
var franceDB = [];
var singDB = [];
var spainDB = [];

let customerTable = $('#table').DataTable({

    columns: [
        { data: 'id' },
        { data: 'firstName' },
        { data: 'lastName' },
        { data: 'phoneNumber' },
        { data: 'address' },
        { data: 'city' },
        { data: 'state' },
        { data: 'postalCode' },
        { data: 'country' },
        { data: 'salesRepEmployeeNumber' },
        { data: 'creditLimit' },
        {
            data: 'Details',
            width: '17%'
        },
        { data: 'Action' }
    ],
    columnDefs: [
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-warning" style="cursor:pointer;"></i>
            || <i class="fas fa-trash text-danger" style="cursor:pointer;"></i>`,
        },
        {
            targets: -2,
            defaultContent: `<button id='btn-order' class='btn btn-info'>Orders</button> <button id='btn-payment' class='btn btn-info'>Payments</button>`,
            width: '15%'
        }
    ],
    buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"]

})

customerTable.buttons().container().appendTo('#table_wrapper .col-md-6:eq(0)');

let _customer = {
    newCustomer: {
        firstName: '',
        lastName: '',
        phoneNumber: '',
        address: '',
        city: '',
        state: '',
        postalCode: '',
        country: '',
        salesRepEmployeeNumber: '',
        creditLimit: ''
    },
    // CREATE - Tạo lớp mới
    onCreateNewCustomerClick() {
        gCustomerId = 0;
        this.newCustomer = {
            firstName: $('#inp-first-name').val().trim(),
            lastName: $('#inp-last-name').val().trim(),
            phoneNumber: $('#inp-phone-number').val().trim(),
            address: $('#inp-address').val().trim(),
            city: $('#inp-city').val().trim(),
            state: $('#inp-state').val().trim(),
            country: $('#inp-country').val().trim(),
            postalCode: $('#inp-postal-code').val().trim(),
            salesRepEmployeeNumber: $('#inp-sales-rep').val().trim(),
            creditLimit: $('#inp-credit-limit').val().trim()
        }
        if (validateCustomer(this.newCustomer)) {
            $.ajax({
                url: `http://localhost:8080/customers`,
                method: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(this.newCustomer),
                success: () => {
                    alert('successfully create new Customer');
                    $.get(`http://localhost:8080/customers`, loadCustomerToTable);
                    resetCustomerInput();
                },
                error: (err) => alert(err.responseText),
            });
        }
    },
    onUpdateModalClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = customerTable.row(vSelectedRow).data();
        gCustomerId = vSelectedData.id;

        $('#modal-update-customer').modal('show');
        $.get(`http://localhost:8080/customers/${gCustomerId}`, loadCustomerToInput);

    },
    onConfirmUpdateClassClick() {
        this.newCustomer = {
            firstName: $('#modal-inp-first-name').val().trim(),
            lastName: $('#modal-inp-last-name').val().trim(),
            phoneNumber: $('#modal-inp-phone-number').val().trim(),
            address: $('#modal-inp-address').val().trim(),
            city: $('#modal-inp-city').val().trim(),
            state: $('#modal-inp-state').val().trim(),
            country: $('#modal-inp-country').val().trim(),
            postalCode: $('#modal-inp-postal-code').val().trim(),
            salesRepEmployeeNumber: $('#modal-inp-sales-rep').val().trim(),
            creditLimit: $('#modal-inp-credit-limit').val().trim()
        };
        if (validateCustomer(this.newCustomer)) {
            $.ajax({
                url: `http://localhost:8080/customers/${gCustomerId}`,
                method: 'PUT',
                contentType: 'application/json',
                data: JSON.stringify(this.newCustomer),
                success: () => {
                    alert('successfully update Customer with id: ' + gCustomerId);
                    $('#modal-update-customer').modal('hide');
                    $.get(`http://localhost:8080/customers`, loadCustomerToTable);
                },
                error: (err) => alert(err.responseText),
            });
        }

    },
    onDeleteCustomerClick() {
        $('#modal-delete-customer').modal('show');
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = customerTable.row(vSelectedRow).data();
        gCustomerId = vSelectedData.id;

    },
    onConfirmDeleteClassClick() {
        $.ajax({
            url: `http://localhost:8080/customers/${gCustomerId}`,
            method: 'delete',
            success: () => {
                alert('successfully delete customer with id: ' + gCustomerId);
                $('#modal-delete-customer').modal('hide');
                $.get(`http://localhost:8080/customers`, loadCustomerToTable);
            },
            error: (err) => alert(err.responseText),
        });
    },
    getOrdersClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = customerTable.row(vSelectedRow).data();
        gCustomerId = vSelectedData.id;
        let url = 'order.html'
            + "?customerId="
            + gCustomerId;
        window.location.href = url;
    },
    getPaymentsClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = customerTable.row(vSelectedRow).data();
        gCustomerId = vSelectedData.id;
        let url = 'payment.html'
            + "?customerId="
            + gCustomerId;
        window.location.href = url;
    }
};

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$.get(`http://localhost:8080/customers`, loadCustomerToTable);


$('#btn-create-customer').click(_customer.onCreateNewCustomerClick);
$('#btn-update-customer').click(_customer.onConfirmUpdateClassClick);
$('#btn-confirm-delete-customer').click(_customer.onConfirmDeleteClassClick);
// $('.buttons-excel').click(excelExporter);

$('#customer-table').on('click', '.fa-edit', _customer.onUpdateModalClick);
$('#customer-table').on('click', '.fa-trash', _customer.onDeleteCustomerClick);
$('#customer-table').on('click', '#btn-order', _customer.getOrdersClick);
$('#customer-table').on('click', '#btn-payment', _customer.getPaymentsClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */

function loadCustomerToTable(paramCustomer) {
    customerTable.clear();
    customerTable.rows.add(paramCustomer);
    customerTable.draw();
}
function loadCustomerToInput(paramCustomer) {
    $('#modal-inp-first-name').val(paramCustomer.firstName);
    $('#modal-inp-last-name').val(paramCustomer.lastName);
    $('#modal-inp-phone-number').val(paramCustomer.phoneNumber);
    $('#modal-inp-address').val(paramCustomer.address);
    $('#modal-inp-city').val(paramCustomer.city);
    $('#modal-inp-state').val(paramCustomer.state);
    $('#modal-inp-country').val(paramCustomer.country);
    $('#modal-inp-postal-code').val(paramCustomer.postalCode);
    $('#modal-inp-sales-rep').val(paramCustomer.salesRepEmployeeNumber);
    $('#modal-inp-credit-limit').val(paramCustomer.creditLimit);
}
function validateCustomer(paramCustomer) {
    let vResult = true;
    try {
        if (paramCustomer.firstName == '') {
            vResult = false;
            throw `first name can't be empty`;
        }

        if (paramCustomer.lastName == '') {
            vResult = false;
            throw `last name can't be empty`;
        }

        if (paramCustomer.phoneNumber == '') {
            vResult = false;
            throw `phone number can't be empty`;
        }

    } catch (e) {
        alert(e);
    }
    return vResult;
}

function resetCustomerInput() {
    $('#inp-first-name').val('');
    $('#inp-last-name').val('');
    $('#inp-phone-number').val('');
    $('#inp-address').val('');
    $('#inp-city').val('');
    $('#inp-state').val('');
    $('#inp-country').val('');
    $('#inp-postal-code').val('');
    $('#inp-sales-rep').val('');
    $('#inp-credit-limit').val('');
}


$.get(`http://localhost:8080/customers2`, function (chartData) {
    console.log(chartData)
    
    var areaChartData = {
        labels: ['France', 'Singapore', 'Spain', 'USA'],
        datasets: [
            { 
                label: 'Customer',
                data: chartData,
                backgroundColor: 'rgba(60,141,188,0.9)',
                borderColor: 'rgba(60,141,188,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)'
            }
        ]
    }


//-------------
//- BAR CHART -
//-------------
var barChartCanvas = $('#barChart').get(0).getContext('2d')
var barChartData = $.extend(true, {}, areaChartData)
var temp0 = areaChartData.datasets
barChartData.datasets = temp0


var barChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
    datasetFill: false
}

new Chart(barChartCanvas, {
    type: 'bar',
    data: barChartData,
    options: barChartOptions
})
});


// function excelExporter() {
//     $.ajax({
//         type: 'GET',
//         cache: false,
//         url: "http://localhost:8080/export/customers/excel",
//         xhrFields: {
//             responseType: 'arraybuffer'
//         }
//     })
//         .done(function (data, status, xmlHeaderRequest) {
//             var downloadLink = document.createElement('a');
//             var blob = new Blob([data],
//                 {
//                     type: xmlHeaderRequest.getResponseHeader('Content-Type')
//                 });
//             var url = window.URL || window.webkitURL;
//             var downloadUrl = url.createObjectURL(blob);
//             var fileName = 'customers';

//             if (typeof window.navigator.msSaveBlob !== 'undefined') {
//                 window.navigator.msSaveBlob(blob, fileName);
//             } else {
//                 if (fileName) {
//                     if (typeof downloadLink.download === 'undefined') {
//                         window.location = downloadUrl;
//                     } else {
//                         downloadLink.href = downloadUrl;
//                         downloadLink.download = fileName;
//                         document.body.appendChild(downloadLink);
//                         downloadLink.click();
//                     }
//                 } else {
//                     window.location = downloadUrl;
//                 }

//                 setTimeout(function () {
//                     url.revokeObjectURL(downloadUrl);
//                 },
//                     100);
//             }
//         });
// }




