//"use strict"

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

$(document).ready(function () {
    onPageLoading();
});

let gCustomerId = 0;
let gOrderId = 0;
let gSelectCustomer = $('#select-customer')

let orderTable = $('#order-table').DataTable({
    ordering: false,
    columns: [
        { data: 'id' },
        {
            data: 'customerFullName',
            width: '15%',

        },
        {
            data: 'orderDate',
            width: '20%',
            render: function (data, type, row) {
                return moment(row.orderDate).format('DD/MM/YYYY hh:mm:ss a');
            }
        },
        {
            data: 'requiredDate',
            width: '20%',
            render: function (data, type, row) {
                return moment(row.requiredDate).format('DD/MM/YYYY hh:mm:ss a');
            }
        },
        {
            data: 'shippedDate',
            width: '3%',
        },
        { data: 'status' },
        { data: 'comments' },
        { data: 'Action' }
    ],
    columnDefs: [
        {
            targets: -1,
            defaultContent: `<i class="fa-solid fa-circle-info text-success" style="cursor:pointer;"></i> | <i class="fas fa-edit text-warning" style="cursor:pointer;"></i>
            | <i class="fas fa-trash text-danger" style="cursor:pointer;"></i>`,
        }
    ],
});

let order = {
    newOrder: {
        orderDate: '',
        requiredDate: '',
        shippedDate: '',
        status: '',
        comments: '',
    },
    onNewOrderClick() {
        gOrderId = 0;
        this.newOrder = {
            orderDate: $('#inp-order-date').val().trim(),
            requiredDate: $('#inp-required-date').val().trim(),
            shippedDate: $('#inp-shipped-date').val().trim(),
            status: $('#inp-status').val().trim(),
            comments: $('#inp-comment').val().trim()
        }
        if (validateOrder(this.newOrder)) {
            if (gCustomerId == 0) {
                alert(`Please select a customer to create new order!`);
            } else {
                $.ajax({
                    url: `http://localhost:8080/customers/${gCustomerId}/orders`,
                    method: 'POST',
                    data: JSON.stringify(this.newOrder),
                    contentType: 'application/json',
                    success: () => {
                        alert(`Order created successfully`);
                        $.get(
                            `http://localhost:8080/customers/${gCustomerId}/orders`,
                            loadOrderToTable,
                        );
                        resetOrderInput();
                    },
                });
            }
        }
    },
    onUpdateModalClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = orderTable.row(vSelectedRow).data();
        gOrderId = vSelectedData.id;
        $('#modal-update-order').modal('show');
        $.get(
            `http://localhost:8080/orders/${gOrderId}`,
            loadOrderToInput
        );
    },
    onConfirmUpdateOrderClick() {
        this.newOrder = {
            orderDate: $('#modal-inp-order-date').val().trim(),
            requiredDate: $('#modal-inp-required-date').val().trim(),
            shippedDate: $('#modal-inp-shipped-date').val().trim(),
            status: $('#modal-inp-status').val().trim(),
            comments: $('#modal-inp-comment').val().trim()
        };
        if (validateOrder(this.newOrder)) {
            $.ajax({
                url: `http://localhost:8080/orders/${gOrderId}`,
                method: 'PUT',
                data: JSON.stringify(this.newOrder),
                contentType: 'application/json',
                success: () => {
                    alert(`Order updated successfully`);
                    $('#modal-update-order').modal('hide');
                    $.get(`http://localhost:8080/orders`, loadOrderToTable);
                },
                error: (err) => alert(err.responseText)
            });
        }
    },
    onDeletePaymentByIdClick() {
        $('#modal-delete-order').modal('show');
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = orderTable.row(vSelectedRow).data();
        gOrderId = vSelectedData.id;
    },
    oConfirmDeleteOrderClick() {
        $.ajax({
            url: `http://localhost:8080/orders/${gOrderId}`,
            method: 'delete',
            success: () => {
                alert(`Order with id ${gOrderId} was successfully deleted`);
                $('#modal-delete-order').modal('hide');
                $.get(`http://localhost:8080/orders`, loadOrderToTable);

            },
            error: (err) => alert(err.responseText),
        });
    },
    onGetOrderDetailsClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = orderTable.row(vSelectedRow).data();
        gOrderId = vSelectedData.id;

        let url = 'orderDetail.html'
            + "?orderId="
            + gOrderId;
        window.location.href = url;
    }
};
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$.get(`http://localhost:8080/customers`, loadCustomerToSelect);


gSelectCustomer.change(onGetCustomerChange);

$('#btn-create-order').click(order.onNewOrderClick);
$('#btn-update-order').click(order.onConfirmUpdateOrderClick);
$('#btn-confirm-delete-order').click(order.oConfirmDeleteOrderClick);
$('#btn-excel-exporter').click(excelExporter);

$('#order-table').on('click', '.fa-edit', order.onUpdateModalClick);
$('#order-table').on('click', '.fa-trash', order.onDeletePaymentByIdClick);
$('#order-table').on('click', '.fa-circle-info', order.onGetOrderDetailsClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Chọn lớp thì load học sinh ra bảng
function onGetCustomerChange(event) {
    gCustomerId = event.target.value;
    if (gCustomerId == 0) {
        $.get(`http://localhost:8080/orders`, loadOrderToTable);
    } else {
        $.get(
            `http://localhost:8080/customers/${gCustomerId}/orders`,
            loadOrderToTable,
        );
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
function loadCustomerToSelect(paramCustomer) {
    paramCustomer.forEach((Customer) => {
        $('<option>', {
            text: Customer.id,
            value: Customer.id,
        }).appendTo(gSelectCustomer);
    });
}
function loadOrderToTable(paramOrder) {
    orderTable
    .clear()
    .rows.add(paramOrder)
    .draw();
}

function validateOrder(paramOrder) {
    let vResult = true;
    try {
        if (paramOrder.orderDate == '') {
            vResult = false;
            throw `Order date can't empty`;
        }
        if (paramOrder.requiredDate == '') {
            vResult = false;
            throw `Required date can't empty`;
        }
    } catch (e) {
        alert(e);
    }
    return vResult;
}
function resetOrderInput() {
    $('#inp-order-date').val('');
    $('#inp-required-date').val('');
    $('#inp-shipped-date').val('');
    $('#inp-status').val('');
    $('#inp-comments').val('');
}
function loadOrderToInput(paramOrder) {
    $('#modal-inp-order-date').val(moment(new Date(paramOrder.orderDate)).format('YYYY-MM-DDTHH:mm'));
    $('#modal-inp-required-date').val(moment(new Date(paramOrder.requiredDate)).format('YYYY-MM-DDTHH:mm'));
    $('#modal-inp-shipped-date').val(moment(new Date(paramOrder.shippedDate)).format('YYYY-MM-DDTHH:mm'));
    $('#modal-inp-status').val(paramOrder.status);
    $('#modal-inp-comment').val(paramOrder.comments);
    //moment(new Date(paramPayment.orderDate)).format('YYYY-MM-DDTHH:mm')
}

function onPageLoading() {
    let urlString = window.location.href;
    let url = new URL(urlString);
    gCustomerId = url.searchParams.get('customerId');
    if (urlString.includes('?')) {
        $.get(`http://localhost:8080/customers`, function () {
            gSelectCustomer.val(gCustomerId);
        });
        $.get(`http://localhost:8080/customers/${gCustomerId}/orders`, loadOrderToTable);
    }
    else {
        $.get(`http://localhost:8080/orders2`, loadOrderToTable);
    }

}

function excelExporter() {
    $.ajax({
        type: 'GET',
        cache: false,
        url: "http://localhost:8080/export/orders/excel",
        xhrFields: {
            responseType: 'arraybuffer'
        }
    })
        .done(function (data, status, xmlHeaderRequest) {
            var downloadLink = document.createElement('a');
            var blob = new Blob([data],
                {
                    type: xmlHeaderRequest.getResponseHeader('Content-Type')
                });
            var url = window.URL || window.webkitURL;
            var downloadUrl = url.createObjectURL(blob);
            var fileName = 'orders';

            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                window.navigator.msSaveBlob(blob, fileName);
            } else {
                if (fileName) {
                    if (typeof downloadLink.download === 'undefined') {
                        window.location = downloadUrl;
                    } else {
                        downloadLink.href = downloadUrl;
                        downloadLink.download = fileName;
                        document.body.appendChild(downloadLink);
                        downloadLink.click();
                    }
                } else {
                    window.location = downloadUrl;
                }

                setTimeout(function () {
                    url.revokeObjectURL(downloadUrl);
                },
                    100);
            }
        });
}

