"use strict"

/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
let gOrderId = 0;
let gOrderDetailId = 0;
let gProductId = 0;
let gSelectOrder = $('#select-order')
let gSelectProduct = $('#select-product');
let gModalSelectProduct = $('#modal-select-product');

let orderDetailTable = $('#order-detail-table').DataTable({
    columns: [
        { data: 'id' },
        { 
            data: 'order.id',
            width: '10%'
        },
        { 
            data: 'productName',
            width: '40%'
        },
        { data: 'quantityOrder' },
        { data: 'priceEach' },
        { data: 'Action' }
    ],
    columnDefs: [
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-warning" style="cursor:pointer;"></i>
            || <i class="fas fa-trash text-danger" style="cursor:pointer;"></i>`,
        }
    ],
});

let orderDetail = {
    newOrderDetail: {
        product: {
            id: ''
        },
        quantityOrder: '',
        priceEach: '',
    },
    onNewOrderDetailClick() {
        gOrderDetailId = 0;
        this.newOrderDetail = {
            product: {
                id: $('#select-product option:selected').val()
            },
            quantityOrder: $('#inp-quantity').val(),
            priceEach: $('#inp-price').val()
        }
        if (validateOrderDetail(this.newOrderDetail)) {
            if (gOrderId == 0) {
                alert(`Please select an order to create new order detail!`);
            } else {
                $.ajax({
                    url: `http://localhost:8080/orders/${gOrderId}/order-details`,
                    method: 'POST',
                    data: JSON.stringify(this.newOrderDetail),
                    contentType: 'application/json',
                    success: () => {
                        alert(`Order detail created successfully`);
                        $.get(
                            `http://localhost:8080/orders/${gOrderId}/order-details`,
                            loadOrderDetailToTable,
                        );
                        resetOrderDetailInput();
                    },
                });
            }
        }
    },
    onUpdateModalClick() {
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = orderDetailTable.row(vSelectedRow).data();
        gOrderDetailId = vSelectedData.id;
        $('#modal-update-order-detail').modal('show');
        $.get(
            `http://localhost:8080/order-details/${gOrderDetailId}`,
            loadOrderDetailToInput
        );
    },
    onConfirmUpdateOrderDetailClick() {
        this.newOrderDetail = {
            product: {
                id: $('#modal-select-product option:selected').val()
            },
            quantityOrder: $('#modal-inp-quantity').val(),
            priceEach: $('#modal-inp-price').val()
        };
        if (validateOrderDetail(this.newOrderDetail)) {
            $.ajax({
                url: `http://localhost:8080/order-details/${gOrderDetailId}`,
                method: 'PUT',
                data: JSON.stringify(this.newOrderDetail),
                contentType: 'application/json',
                success: () => {
                    alert(`Order detail updated successfully`);
                    $('#modal-update-order-detail').modal('hide');
                    $.get(`http://localhost:8080/order-details`, loadOrderDetailToTable);
                },
                error: (err) => alert(err.responseText)
            });
        }
    },
    onDeleteOrderDetailByIdClick() {
        $('#modal-delete-order-detail').modal('show');
        let vSelectedRow = $(this).parents('tr');
        let vSelectedData = orderDetailTable.row(vSelectedRow).data();
        gOrderDetailId = vSelectedData.id;
    },
    oConfirmDeleteOrderDetailClick() {
        $.ajax({
            url: `http://localhost:8080/order-details/${gOrderDetailId}`,
            method: 'delete',
            success: () => {
                alert(`Order detail with id ${gOrderDetailId} was successfully deleted`);
                $('#modal-delete-order-detail').modal('hide');
                $.get(`http://localhost:8080/order-details`, loadOrderDetailToTable);

            },
            error: (err) => alert(err.responseText),
        });
    },
};
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$.get(`http://localhost:8080/orders`, loadOrderToSelect);
$.get(`http://localhost:8080/products`, loadProductToSelect);
onPageLoading();

gSelectOrder.change(onGetOrderChange);
gSelectProduct.change(onGetProductChange);
gModalSelectProduct.change(onModalGetProductChange);

$('#btn-create-order-detail').click(orderDetail.onNewOrderDetailClick);
$('#btn-update-order-detail').click(orderDetail.onConfirmUpdateOrderDetailClick);
$('#btn-confirm-delete-order-detail').click(orderDetail.oConfirmDeleteOrderDetailClick);

$('#order-detail-table').on('click', '.fa-edit', orderDetail.onUpdateModalClick);
$('#order-detail-table').on('click', '.fa-trash', orderDetail.onDeleteOrderDetailByIdClick);
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */

function onGetOrderChange(event) {
    gOrderId = event.target.value;
    if (gOrderId == 0) {
        $.get(`http://localhost:8080/order-details`, loadOrderDetailToTable);
    } else {
        $.get(
            `http://localhost:8080/orders/${gOrderId}/order-details`,
            loadOrderDetailToTable,
        );
    }
}

function onGetProductChange(event) {
    gProductId = event.target.value;
    $.get(
        `http://localhost:8080/products/${gProductId}`,
        loadProductToInput,
    );
}

function onModalGetProductChange(event) {
    gProductId = event.target.value;
    $.get(
        `http://localhost:8080/products/${gProductId}`,
        loadProductToModalInput,
    );
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
function loadOrderToSelect(paramOrder) {
    paramOrder.forEach((Order) => {
        $('<option>', {
            text: Order.id,
            value: Order.id,
        }).appendTo(gSelectOrder);
    });
}
function loadProductToSelect(paramProduct) {
    paramProduct.forEach((Product) => {
        $('<option>', {
            text: Product.code,
            value: Product.id,
        }).appendTo(gSelectProduct);
    });
}

function loadProductToSelectModal(paramProduct) {
    paramProduct.forEach((Product) => {
        $('<option>', {
            text: Product.code,
            value: Product.id,
        }).appendTo(gModalSelectProduct);
    });
}

function loadProductToInput(paramProduct) {
    $('#inp-product-name').val(paramProduct.name);
    $('#inp-product-desc').val(paramProduct.description);

}
function loadProductToModalInput(paramProduct) {
    $('#modal-inp-product-name').val(paramProduct.name);
    $('#modal-inp-product-desc').val(paramProduct.description);
}
function loadOrderDetailToTable(paramOrderDetail) {
    orderDetailTable.clear();
    orderDetailTable.rows.add(paramOrderDetail);
    orderDetailTable.draw();
}

function validateOrderDetail(paramOrderDetail) {
    let vResult = true;
    try {
        if (paramOrderDetail.product.id == '') {
            vResult = false;
            throw `Product id can't empty`;
        }
        if (paramOrderDetail.quantityOrder == '') {
            vResult = false;
            throw `Quantity can't empty`;
        }

        if (paramOrderDetail.priceEach == '') {
            vResult = false;
            throw `Price can't empty`;
        }
    } catch (e) {
        alert(e);
    }
    return vResult;
}
function resetOrderDetailInput() {
    $('#select-product').val('');
    $('#inp-quantity').val('');
    $('#inp-product-name').val('');
    $('#inp-price').val('');
    $('#inp-product-desc').val('');
}
function loadOrderDetailToInput(paramOrderDetail) {
    $.get(`http://localhost:8080/products`, loadProductToSelectModal);
    $('#modal-select-product').val(paramOrderDetail.product.id);
    $('#modal-inp-product-name').val(paramOrderDetail.product.name);
    $('#modal-inp-quantity').val(paramOrderDetail.quantityOrder);
    $('#modal-inp-price').val(paramOrderDetail.priceEach);
    $('#modal-inp-product-desc').val(paramOrderDetail.product.description);
}

function onPageLoading() {
    let urlString = window.location.href;
    let url = new URL(urlString);
    gOrderId = url.searchParams.get('orderId');
    if (urlString.includes('?')) {
        $.get(`http://localhost:8080/orders`, function() {
            gSelectOrder.val(gOrderId);
        });
        $.get(
            `http://localhost:8080/orders/${gOrderId}/order-details`,
            loadOrderDetailToTable,
        );
    }
    else {
        $.get(`http://localhost:8080/order-details`, loadOrderDetailToTable);
    }
}