package com.devcamp.j28.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.devcamp.j28.entity.Office;
import com.devcamp.j28.repository.iOfficeRepository;
import com.devcamp.j28.service.OfficeService;

@Transactional
@RestController
public class OfficeController {
    @Autowired
    private iOfficeRepository officeRepository;

    @Autowired
    private OfficeService officeService;

    // 1. Get all
    @CrossOrigin
    @GetMapping("/offices")
    public List<Office> getAllOffices() {
        return officeRepository.findAll();
    }

    // 2. Get details
    @CrossOrigin
    @GetMapping("/offices/{id}")
    public Office getOfficeById(@PathVariable int id) {
        if (officeRepository.findById(id).isPresent())
            return officeRepository.findById(id).get();
        else
            return null;
    }

    // 3. Create
    @CrossOrigin
    @PostMapping("/offices")
    public ResponseEntity<Object> createCountry(@RequestBody Office office) {
        try {
            return new ResponseEntity<>(officeService.newOffice(office), HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified Office: "
                    + e.getCause().getCause().getMessage());
        }
    }

    // 4. Update
    @CrossOrigin
    @PutMapping("/offices/{id}")
    public ResponseEntity<Object> updateOffice(@PathVariable("id") int id, @RequestBody Office office) {
        try {
            Optional<Office> OfficeData = officeRepository.findById(id);
            if (OfficeData.isPresent()) {
                Office newRole = OfficeData.get();
                newRole.setCity(office.getCity());
                newRole.setPhone(office.getPhone());
                newRole.setAddressLine(office.getAddressLine());
                newRole.setState(office.getState());
                newRole.setCountry(office.getCountry());
                newRole.setTerritory(office.getTerritory());
                Office savedRole = officeRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // 5. Delete
    @CrossOrigin
    @DeleteMapping("/offices/{id}")
    public ResponseEntity<Object> deleteOfficeById(@PathVariable int id) {
        try {
            officeRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
