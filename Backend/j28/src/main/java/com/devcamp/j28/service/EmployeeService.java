package com.devcamp.j28.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.j28.entity.Employee;
import com.devcamp.j28.repository.iEmployeeRepository;

@Service
public class EmployeeService {
    @Autowired
    private iEmployeeRepository employeeRepository;

    public Employee newEmployee(Employee employee) {
        Employee newRole = new Employee();
        newRole.setLastName(employee.getLastName());
        newRole.setFirstName(employee.getFirstName());
        newRole.setExtension(employee.getExtension());
        newRole.setEmail(employee.getEmail());
        newRole.setOfficeCode(employee.getOfficeCode());
        newRole.setReportTo(employee.getReportTo());
        newRole.setJobTitle(employee.getJobTitle());
        Employee savedRole = employeeRepository.save(newRole);
        return savedRole;
    }

}
