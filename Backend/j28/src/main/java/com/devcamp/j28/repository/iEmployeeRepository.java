package com.devcamp.j28.repository;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.devcamp.j28.entity.Employee;

@Repository
public interface iEmployeeRepository extends JpaRepository<Employee, Long>{
    @Query(value = "SELECT * FROM employees WHERE last_name LIKE %lastName%", nativeQuery = true)
    List<Employee> findEmployeeByEmployeeLastName(@Param("lastName") String lastName);

    @Query(value = "SELECT * FROM employees WHERE first_name LIKE %firstName%", nativeQuery = true)
    List<Employee> findEmployeeByEmployeeFirstName(@Param("firstName") String firstName);

    Optional<Employee> findById(int id);

    void deleteById(int id);
}
