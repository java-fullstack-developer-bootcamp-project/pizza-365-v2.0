package com.devcamp.j28.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.devcamp.j28.entity.Customer;
import com.devcamp.j28.entity.Order;
import com.devcamp.j28.repository.iCustomerRepository;
import com.devcamp.j28.repository.iOrderRepository;
import com.devcamp.j28.service.OrderExcelExporter;

@Transactional
@RestController
public class OrderController {
    @Autowired
    private iOrderRepository orderRepository;

    @Autowired
    private iCustomerRepository customerRepository;

    // 1. Get all
    @CrossOrigin
    @GetMapping("/orders")
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }

    @CrossOrigin
    @GetMapping("/orders2")
    public List<Order> getAllOrdersBy() {
        return orderRepository.findOrdersOrderByCustomerFullName();
    }
    // 2. Get details
    @CrossOrigin
    @GetMapping("/orders/{id}")
    public Order getOrderById(@PathVariable int id) {
        if (orderRepository.findById(id).isPresent())
            return orderRepository.findById(id).get();
        else
            return null;
    }

    // 2.2 Get all by customerId
	@CrossOrigin
    @GetMapping("/customers/{customerId}/orders")
    public List<Order> getOrdersByCustomerId(@PathVariable(value = "customerId") int customerId) {
        return orderRepository.findByCustomerId(customerId);
    }

    // 3. Create
    @CrossOrigin
    @PostMapping("/customers/{customerId}/orders")
    public ResponseEntity<Object> createOrder(@PathVariable("customerId") int customerId, @RequestBody Order order) {
        try {
            Optional<Customer> customerData = customerRepository.findById(customerId);
            if (customerData.isPresent()) {
                Order newRole = new Order();
                newRole.setComments(order.getComments());
                newRole.setOrderDate(order.getOrderDate());
                newRole.setRequiredDate(order.getRequiredDate());
                newRole.setShippedDate(order.getShippedDate());
                newRole.setStatus(order.getStatus());
                newRole.setOrderDetails(order.getOrderDetails());
                newRole.setCustomer(order.getCustomer());

                Customer _customer = customerData.get();
                newRole.setCustomer(_customer);
                
                Order savedRole = orderRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified Order: "
                    + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // 4. Update
    @CrossOrigin
    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable("id") int id, @RequestBody Order order) {
        try {
            Optional<Order> orderData = orderRepository.findById(id);
            if (orderData.isPresent()) {
                Order newRole = orderData.get();
                newRole.setComments(order.getComments());
                newRole.setOrderDate(order.getOrderDate());
                newRole.setRequiredDate(order.getRequiredDate());
                newRole.setShippedDate(order.getShippedDate());
                newRole.setStatus(order.getStatus());
                // newRole.setOrderDetails(order.getOrderDetails());
                //newRole.setCustomer(order.getCustomer());
                Order savedRole = orderRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // 5. Delete
    @CrossOrigin
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Object> deleteOrderById(@PathVariable int id) {
        try {
            orderRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // 6. ExcelExporter
    @CrossOrigin
    @GetMapping("/export/orders/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);

		List<Order> order = new ArrayList<Order>();

		orderRepository.findAll().forEach(order::add);

		OrderExcelExporter excelExporter = new OrderExcelExporter(order);

		excelExporter.export(response);
	}
}
