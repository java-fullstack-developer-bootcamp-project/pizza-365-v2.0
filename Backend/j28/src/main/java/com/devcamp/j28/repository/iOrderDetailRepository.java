package com.devcamp.j28.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.j28.entity.Order;
import com.devcamp.j28.entity.OrderDetail;

public interface iOrderDetailRepository extends JpaRepository<OrderDetail, Long> {
    @Query(value = "SELECT * FROM order_details WHERE order_id = :order", nativeQuery = true)
    List<OrderDetail> findOrderDetailByOrderId(@Param("order") String order);

    @Query(value = "SELECT * FROM order_details WHERE product_id = :product", nativeQuery = true)
    List<OrderDetail> findOrderDetailByProductId(@Param("product") String product);

    Optional<OrderDetail> findById(int id);

    void deleteById(int id);

    List<OrderDetail> findByOrderId(int orderId);
}
