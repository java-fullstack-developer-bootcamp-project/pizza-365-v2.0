package com.devcamp.j28.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.devcamp.j28.entity.Order;
import com.devcamp.j28.entity.OrderDetail;
import com.devcamp.j28.repository.iOrderDetailRepository;
import com.devcamp.j28.repository.iOrderRepository;

@Transactional
@RestController
public class OrderDetailController {
	@Autowired
	private iOrderDetailRepository orderDetailRepository;

	@Autowired
	private iOrderRepository orderRepository;

	// 1. Get all
	@CrossOrigin
	@GetMapping("/order-details")
	public List<OrderDetail> getAllOrderDetails() {
		return orderDetailRepository.findAll();
	}

	// 2. Get details
	@CrossOrigin
	@GetMapping("/order-details/{id}")
	public OrderDetail getOrderDetailById(@PathVariable int id) {
		if (orderDetailRepository.findById(id).isPresent())
			return orderDetailRepository.findById(id).get();
		else
			return null;
	}

	// 2.2 Get all by orderId
	@CrossOrigin
    @GetMapping("/orders/{orderId}/order-details")
    public List<OrderDetail> getOrderDetailsByOrderId(@PathVariable(value = "orderId") int orderId) {
        return orderDetailRepository.findByOrderId(orderId);
    }

	// 3. Create
	@CrossOrigin
	@PostMapping("/orders/{orderId}/order-details")
	public ResponseEntity<Object> createOrderDetail(@PathVariable("orderId") int orderId, @RequestBody OrderDetail orderDetail) {
		try {
			Optional<Order> OrderData = orderRepository.findById(orderId);
			if (OrderData.isPresent()) {
				OrderDetail newRole = new OrderDetail();
				newRole.setOrder(orderDetail.getOrder());
				newRole.setProduct(orderDetail.getProduct());
				newRole.setQuantityOrder(orderDetail.getQuantityOrder());
				newRole.setPriceEach(orderDetail.getPriceEach());

				Order _order = OrderData.get();
				newRole.setOrder(_order);

				OrderDetail savedRole = orderDetailRepository.save(newRole);
				return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified OrderDetail: "
					+ e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	// 4. Update
	@CrossOrigin
	@PutMapping("/order-details/{id}")
	public ResponseEntity<Object> updateOrderDetail(@PathVariable("id") int id, @RequestBody OrderDetail orderDetail) {
		Optional<OrderDetail> orderDetailData = orderDetailRepository.findById(id);
		if (orderDetailData.isPresent()) {
			OrderDetail newRole = orderDetailData.get();
			//newRole.setOrder(orderDetail.getOrder());
			newRole.setProduct(orderDetail.getProduct());
			newRole.setQuantityOrder(orderDetail.getQuantityOrder());
			newRole.setPriceEach(orderDetail.getPriceEach());
			OrderDetail savedRole = orderDetailRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	// 5. Delete
	@CrossOrigin
	@DeleteMapping("/order-details/{id}")
	public ResponseEntity<Object> deleteOrderDetailById(@PathVariable int id) {
		try {
			orderDetailRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
