package com.devcamp.j28.repository;

import java.util.Optional;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.j28.entity.Product;

@Repository
public interface iProductRepository extends JpaRepository<Product, Long> {
    Optional<Product> findById(int id);
    void deleteById(int id);
    
    @Query(value = "SELECT * FROM products WHERE product_code LIKE %code%", nativeQuery = true)
    List<Product> findProductByProductCodeLike(@Param("code") String code);

    @Query(value = "SELECT * FROM products WHERE product_name LIKE %name%", nativeQuery = true)
    List<Product> findCustomerByProductNameLike(@Param("name") String name);
    List<Product> findByProductLineId(int id);
}
