package com.devcamp.j28.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.j28.entity.Customer;
import com.devcamp.j28.repository.iCustomerRepository;

@Service
public class CustomerService {
    @Autowired
    private iCustomerRepository customerRepository;

    public Customer newCustomer(Customer customer) {
        Customer newRole = new Customer();
        newRole.setFirstName(customer.getFirstName());
        newRole.setLastName(customer.getLastName());
        newRole.setAddress(customer.getAddress());
        newRole.setCity(customer.getCity());
        newRole.setCountry(customer.getCountry());
        newRole.setCreditLimit(customer.getCreditLimit());
        newRole.setPhoneNumber(customer.getPhoneNumber());
        newRole.setPostalCode(customer.getPostalCode());
        newRole.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
        newRole.setState(customer.getState());
        newRole.setOrders(customer.getOrders());
        newRole.setPayments(customer.getPayments());
        Customer savedRole = customerRepository.save(newRole);
        return savedRole;
    }
}
