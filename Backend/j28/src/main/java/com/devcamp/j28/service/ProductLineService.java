package com.devcamp.j28.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.j28.entity.ProductLine;
import com.devcamp.j28.repository.iProductLineRepository;

@Service
public class ProductLineService {
    @Autowired
    private iProductLineRepository productLineRepository;

    public ProductLine newProductLine(ProductLine productLine) {
        ProductLine newRole = new ProductLine();
		newRole.setProductLine(productLine.getProductLine());
		newRole.setDescription(productLine.getDescription());
		newRole.setProducts(productLine.getProducts());
		ProductLine savedRole = productLineRepository.save(newRole);
        return savedRole;
    }
}
