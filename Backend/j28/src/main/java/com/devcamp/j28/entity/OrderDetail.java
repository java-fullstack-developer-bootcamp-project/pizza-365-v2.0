package com.devcamp.j28.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;

@Entity
@Table(name="order_details")
public class OrderDetail {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
	private Order order;

	@ManyToOne
	private Product product;

	@Column(name = "quantity_order")
	private int quantityOrder;

	@Column(name = "price_each")
	private BigDecimal priceEach;

	@Transient
    private String productName;

    public String getProductName() {
		return getProduct().getName();
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public OrderDetail() {
    }

    public OrderDetail(int id, int quantityOrder, BigDecimal priceEach) {
		super();
		this.id = id;
		this.quantityOrder = quantityOrder;
		this.priceEach = priceEach;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantityOrder() {
		return quantityOrder;
	}

	public void setQuantityOrder(int quantityOrder) {
		this.quantityOrder = quantityOrder;
	}

	public BigDecimal getPriceEach() {
		return priceEach;
	}

	public void setPriceEach(BigDecimal priceEach) {
		this.priceEach = priceEach;
	}
    
}
