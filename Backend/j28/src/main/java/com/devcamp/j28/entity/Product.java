package com.devcamp.j28.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="products")
public class Product {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

    @Column(name="product_code")
    private String code;

    @Column(name="product_name")
    private String name;

    @Column(name="product_description")
    private String description;

    @Column(name="product_scale")
    private String scale;

    @Column(name="product_vendor")
    private String vendor;

    @Column(name="quantity_in_stock")
    private int quantityInStock;

    @Column(name="buy_price")
    private BigDecimal buyPrice;

    @ManyToOne

    private ProductLine productLine;

    @OneToMany(targetEntity = OrderDetail.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "product_id")
    @JsonIgnore
    private List<OrderDetail> orderDetails;


    public Product() {
    }

    public Product(int id, String code, String name, String description, String scale, String vendor,
            int quantityInStock, BigDecimal buyPrice, ProductLine productLine) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.description = description;
        this.scale = scale;
        this.vendor = vendor;
        this.quantityInStock = quantityInStock;
        this.buyPrice = buyPrice;
        this.productLine = productLine;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public BigDecimal getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(BigDecimal buyPrice) {
        this.buyPrice = buyPrice;
    }

    public ProductLine getProductLine() {
        return productLine;
    }

    public void setProductLine(ProductLine productLine) {
        this.productLine = productLine;
    }

    @JsonIgnore
    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }
    
}
