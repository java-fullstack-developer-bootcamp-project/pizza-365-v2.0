package com.devcamp.j28.repository;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.j28.entity.ProductLine;

@Repository
public interface iProductLineRepository extends JpaRepository<ProductLine, Long> {
    @Query(value = "SELECT * FROM product_lines WHERE product_line LIKE %productLine%", nativeQuery = true)
    List<ProductLine> findProductLineByProductLineLike(@Param("productLine") String productLine);

    Optional<ProductLine> findById(int id);

    void deleteById(int id);
}
