package com.devcamp.j28.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.devcamp.j28.entity.ProductLine;
import com.devcamp.j28.repository.iProductLineRepository;
import com.devcamp.j28.service.ProductLineService;

@Transactional
@RestController
public class ProductLineController {
	@Autowired
	private iProductLineRepository productLineRepository;

	@Autowired
	private ProductLineService productLineService;

	// 1. Get all
	@CrossOrigin
	@GetMapping("/product-lines")
	public List<ProductLine> getAllProductLines() {
		return productLineRepository.findAll();
	}

	// 2. Get details
	@CrossOrigin
	@GetMapping("/product-lines/{id}")
	public ProductLine getProductLineById(@PathVariable int id) {
		if (productLineRepository.findById(id).isPresent())
			return productLineRepository.findById(id).get();
		else
			return null;
	}

	// 3. Create
	@CrossOrigin
	@PostMapping("/product-lines")
	public ResponseEntity<Object> createProductLine(@RequestBody ProductLine productLine) {
		try {
			return new ResponseEntity<>(productLineService.newProductLine(productLine), HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified ProductLine: "
					+ e.getCause().getCause().getMessage());
		}
	}

	// 4. Update
	@CrossOrigin
	@PutMapping("/product-lines/{id}")
	public ResponseEntity<Object> updateProductLine(@PathVariable("id") int id, @RequestBody ProductLine productLine) {
		try {
			Optional<ProductLine> productLineData = productLineRepository.findById(id);
			if (productLineData.isPresent()) {
				ProductLine newRole = productLineData.get();
				newRole.setProductLine(productLine.getProductLine());
				newRole.setDescription(productLine.getDescription());
				newRole.setProducts(productLine.getProducts());
				ProductLine savedRole = productLineRepository.save(newRole);
				return new ResponseEntity<>(savedRole, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// 5. Delete
	@CrossOrigin
	@DeleteMapping("/product-lines/{id}")
	public ResponseEntity<Object> deleteCProductLineById(@PathVariable int id) {
		try {
			productLineRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
