package com.devcamp.j28.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.devcamp.j28.entity.Payment;

public interface iPaymentRepository extends JpaRepository<Payment, Long> {
    @Query(value = "SELECT * FROM payments WHERE check_number LIKE %:checkNumber%", nativeQuery = true)
	List<Payment> findPaymentByCheckNumberLike(@Param("checkNumber") String checkNumber);

    @Query(value = "SELECT * FROM payments WHERE customer_id LIKE :customer%", nativeQuery = true)
	List<Payment> findPaymentByCustomerIdLike(@Param("customer") String customer);

    Optional<Payment> findById(int id);

    void deleteById(int id);

    List<Payment> findByCustomerId(int customerId);
}


