package com.devcamp.j28.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.j28.entity.Office;
import com.devcamp.j28.repository.iOfficeRepository;

@Service
public class OfficeService {
    @Autowired
    private iOfficeRepository officeRepository;

    public Office newOffice(Office office) {
        Office newRole = new Office();
        newRole.setCity(office.getCity());
        newRole.setPhone(office.getPhone());
        newRole.setAddressLine(office.getAddressLine());
        newRole.setState(office.getState());
        newRole.setCountry(office.getCountry());
        newRole.setTerritory(office.getTerritory());
        Office savedRole = officeRepository.save(newRole);
        return savedRole;
    }
}
