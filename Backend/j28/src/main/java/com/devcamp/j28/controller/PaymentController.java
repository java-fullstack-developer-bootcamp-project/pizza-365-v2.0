package com.devcamp.j28.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.devcamp.j28.entity.Customer;
import com.devcamp.j28.entity.Payment;
import com.devcamp.j28.repository.iCustomerRepository;
import com.devcamp.j28.repository.iPaymentRepository;
// import com.devcamp.j28.service.PaymentService;

@Transactional
@RestController
public class PaymentController {
	@Autowired
	private iPaymentRepository paymentRepository;

	@Autowired
	private iCustomerRepository customerRepository;

	// @Autowired
	// private PaymentService paymentService;

	// 1. Get all
	@CrossOrigin
	@GetMapping("/payments")
	public List<Payment> getAllPayments() {
		return paymentRepository.findAll();
	}

	// 2. Get details
	@CrossOrigin
	@GetMapping("/payments/{id}")
	public Payment getPaymentById(@PathVariable int id) {
		if (paymentRepository.findById(id).isPresent())
			return paymentRepository.findById(id).get();
		else
			return null;
	}

	// 2.2 Get all payments by customerId
	@CrossOrigin
	@GetMapping("/customers/{customerId}/payments")
	public List<Payment> getPaymentsByCustomerId(@PathVariable(value = "customerId") int customerId) {
		return paymentRepository.findByCustomerId(customerId);
	}

	// 3. Create
	@CrossOrigin
	@PostMapping("/customers/{customerId}/payments")
	public ResponseEntity<Object> createPayment(@PathVariable("customerId") int customerId,
			@RequestBody Payment payment) {
		try {
			Optional<Customer> customerData = customerRepository.findById(customerId);
			if (customerData.isPresent()) {
				Payment newRole = new Payment();
				newRole.setCustomer(payment.getCustomer());
				newRole.setCheckNumber(payment.getCheckNumber());
				newRole.setPaymentDate(payment.getPaymentDate());
				newRole.setAmmount(payment.getAmmount());

				Customer _customer = customerData.get();
				newRole.setCustomer(_customer);

				Payment savedRole = paymentRepository.save(newRole);
				return new ResponseEntity<>(savedRole, HttpStatus.CREATED);

			}

		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Payment: "
					+ e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	// 4. Update
	@CrossOrigin
	@PutMapping("/payments/{id}")
	public ResponseEntity<Object> updatePayment(@PathVariable("id") int id, @RequestBody Payment payment) {
		try {
			Optional<Payment> paymentData = paymentRepository.findById(id);
			if (paymentData.isPresent()) {
				Payment newRole = paymentData.get();
				//newRole.setCustomer(payment.getCustomer());
				newRole.setCheckNumber(payment.getCheckNumber());
				newRole.setPaymentDate(payment.getPaymentDate());
				newRole.setAmmount(payment.getAmmount());
				Payment savedRole = paymentRepository.save(newRole);
				return new ResponseEntity<>(savedRole, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	// 5. Delete
	@CrossOrigin
	@DeleteMapping("/payments/{id}")
	public ResponseEntity<Object> deletePaymentById(@PathVariable int id) {
		try {
			paymentRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
