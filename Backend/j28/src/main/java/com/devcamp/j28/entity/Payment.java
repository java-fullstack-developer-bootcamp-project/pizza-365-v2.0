package com.devcamp.j28.entity;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

// import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.util.Date;


@Entity
@Table(name="payments")
public class Payment {
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@ManyToOne
    //@JsonIgnore
	private Customer customer;

	@Column(name = "check_number")
	private String checkNumber;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "payment_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date paymentDate;

	private BigDecimal ammount;


    @Transient
    private String customerFullName;

    public String getCustomerFullName() {
        return getCustomer().getFirstName() + " " + getCustomer().getLastName();
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public Payment() {
    }

    public Payment(int id, BigDecimal ammount, String checkNumber, Date paymentDate, Customer customer) {
        this.id = id;
        this.ammount = ammount;
        this.checkNumber = checkNumber;
        this.paymentDate = paymentDate;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getAmmount() {
        return ammount;
    }

    public void setAmmount(BigDecimal ammount) {
        this.ammount = ammount;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    //@JsonIgnore
    public Customer getCustomer() {
        return customer;
    }
    
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    
}
