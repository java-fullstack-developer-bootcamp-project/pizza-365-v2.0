package com.devcamp.j28.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.j28.entity.Order;

public interface iOrderRepository extends JpaRepository<Order, Long> {
    @Query(value = "SELECT * FROM orders WHERE status LIKE :status%", nativeQuery = true)
    List<Order> findOrderByOrderStatusLike(@Param("status") String status);

    @Query(value = "SELECT * FROM orders WHERE customer_id = :customer", nativeQuery = true)
    List<Order> finOrderByCustomerId(@Param("customer") String customer);

    @Transactional
    @Modifying
    @Query(value = "UPDATE orders SET status = :status WHERE status IS NULL", nativeQuery = true)
    int updateStatus(@Param("status") String status);

    Optional<Order> findById(int id);

    void deleteById(int id);

    List<Order> findByCustomerId(int customerId);

    @Query(value = "SELECT * FROM orders LEFT JOIN customers ON orders.customer_id = customers.id ORDER BY customers.first_name ASC", nativeQuery = true)
	List<Order> findOrdersOrderByCustomerFullName();
}
