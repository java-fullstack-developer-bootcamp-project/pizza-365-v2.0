package com.devcamp.j28.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.j28.entity.Customer;

@Repository
public interface iCustomerRepository extends JpaRepository<Customer, Long> {
    void deleteById(int id);

    Optional<Customer> findById(int id);

    // Viết query cho bảng customers cho phép tìm danh sách theo họ hoặc tên với
    // LIKE
    @Query(value = "SELECT * FROM customers WHERE last_name LIKE :lastName%", nativeQuery = true)
    List<Customer> findCustomerByCustomerLastName(@Param("lastName") String lastName);

    @Query(value = "SELECT * FROM customers WHERE first_name LIKE :firstName%", nativeQuery = true)
    List<Customer> findCustomerByCustomerFirstName(@Param("firstName") String firstName);

    // Viết query cho bảng customers cho phép tìm danh sách theo city, state với
    // LIKE có phân trang.
    @Query(value = "SELECT * FROM customers WHERE city LIKE :city% AND state LIKE :state%", nativeQuery = true)
    List<Customer> findCustomerByCustomerCityAndCustomerStateLike(@Param("city") String city,
            @Param("state") String state, Pageable pageable);

    // Viết query cho bảng customers cho phép tìm danh sách theo country có phân
    // trang và ORDER BY tên.
    @Query(value = "SELECT * FROM customers WHERE country like :name1 ORDER BY first_name", nativeQuery = true)
    List<Customer> findCustomerByCustomerCountryDesc(@Param("name1") String name1, Pageable pageable);

    // Viết query cho bảng customers cho phép UPDATE dữ liệu có country = NULL với
    @Transactional
    @Modifying
    @Query(value = "UPDATE customers SET country = :country WHERE country IS NULL", nativeQuery = true)
    String updateCountry(@Param("country") String country);

    // Viết câu lệnh SQL lấy dữ liệu số lượng khách hàng tương ứng với các quốc gia USA, France, Singapore, Spain
    @Query(value = "SELECT COUNT(id), country FROM customers WHERE country IN ('USA', 'France', 'Singapore', 'Spain') GROUP BY country", nativeQuery = true)
    List<Integer> countCustomerByCountry();
}
