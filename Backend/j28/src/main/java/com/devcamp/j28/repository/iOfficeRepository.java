package com.devcamp.j28.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.j28.entity.Office;

@Repository
public interface iOfficeRepository extends JpaRepository<Office, Long> {
    @Query(value = "SELECT * FROM offices WHERE city LIKE %city%", nativeQuery = true)
	List<Office> findOfficeByOfficeCityLike(@Param("city") String city);

    @Query(value = "SELECT * FROM offices WHERE city LIKE %country%", nativeQuery = true)
	List<Office> findOfficeByOfficeCountryLike(@Param("country") String country);

    @Transactional
    @Modifying
    @Query(value = "UPDATE offices SET country = :country WHERE id  = :id", nativeQuery = true)
    String updateCountry(@Param("country") String country, @Param("id") int id);

    Optional<Office> findById(int id);

    void deleteById(int id);

}
