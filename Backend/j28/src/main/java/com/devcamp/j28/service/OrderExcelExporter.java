package com.devcamp.j28.service;

import java.util.List;
import java.io.IOException;
import java.sql.Timestamp;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import com.devcamp.j28.entity.Order;

public class OrderExcelExporter {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<Order> orders;

    public OrderExcelExporter(List<Order> orders) {
        this.orders = orders;
        this.workbook = new XSSFWorkbook();
    }

    /**
     * Tạo các ô cho excel file.
     * 
     * @param row
     * @param columnCount
     * @param value
     * @param style
     */
    private void createCells(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Timestamp) {
            cell.setCellValue((Timestamp) value);
        }
        
        else {
            cell.setCellValue((String) value);
        }
        
        cell.setCellStyle(style);
    }

    /**
     * Khai báo cho sheet và các dòng đầu tiên
     */
    private void writeHeaderLine() {
        this.sheet = workbook.createSheet("Orders");

        Row row = this.sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        createCells(row, 0, "Order ID", style);
        createCells(row, 1, "Customer", style);
        createCells(row, 2, "Order date", style);
        createCells(row, 3, "Required date", style);
        createCells(row, 4, "Shipped date", style);
        createCells(row, 5, "Status", style);
        createCells(row, 6, "Comment", style);
        createCells(row, 7, "Action", style);
    }

    /**
     * fill dữ liệu cho các dòng tiếp theo.
     */
    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (Order order : this.orders) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCells(row, columnCount++, order.getId(), style);
            createCells(row, columnCount++, order.getCustomerFullName(), style);
            createCells(row, columnCount++, order.getOrderDate(), style);
            createCells(row, columnCount++, order.getRequiredDate(), style);
            createCells(row, columnCount++, order.getShippedDate(), style);
            createCells(row, columnCount++, order.getStatus(), style);
            createCells(row, columnCount++, order.getComments(), style);
            createCells(row, columnCount++, " ", style);

        }
    }

    /**
	 * xuất dữ liệu ra dạng file
	 * @param response
	 * @throws IOException
	 */
	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}

}
