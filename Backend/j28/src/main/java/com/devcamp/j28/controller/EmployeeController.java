package com.devcamp.j28.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.devcamp.j28.entity.Employee;
import com.devcamp.j28.repository.iEmployeeRepository;
import com.devcamp.j28.service.EmployeeService;

@Transactional
@RestController
public class EmployeeController {
    @Autowired
    private iEmployeeRepository employeeRepository;

    @Autowired
    private EmployeeService employeeService;

    // 1. Get all
    @CrossOrigin
    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    // 2. Get details
    @CrossOrigin
    @GetMapping("/employees/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        if (employeeRepository.findById(id).isPresent())
            return employeeRepository.findById(id).get();
        else
            return null;
    }

    // 3. Create
    @CrossOrigin
	@PostMapping("/employees")
	public ResponseEntity<Object> createEmployee(@RequestBody Employee employee) {
		try {
			return new ResponseEntity<>(employeeService.newEmployee(employee), HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Employee: "
			+e.getCause().getCause().getMessage());
		}
	}

    // 4. Update
    @CrossOrigin
	@PutMapping("/employees/{id}")
	public ResponseEntity<Object> updateEmployee(@PathVariable("id") int id, @RequestBody Employee employee) {
        try {
            Optional<Employee> employeeData = employeeRepository.findById( id);
		if (employeeData.isPresent()) {
			Employee newRole = employeeData.get();
            newRole.setLastName(employee.getLastName());
            newRole.setFirstName(employee.getFirstName());
            newRole.setExtension(employee.getExtension());
            newRole.setEmail(employee.getEmail());
            newRole.setOfficeCode(employee.getOfficeCode());
            newRole.setReportTo(employee.getReportTo());
            newRole.setJobTitle(employee.getJobTitle());
			Employee savedRole = employeeRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
		
	}


    // 5. Delete
    @CrossOrigin
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<Object> deleteEmployeeById(@PathVariable int id) {
		try {
			employeeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
