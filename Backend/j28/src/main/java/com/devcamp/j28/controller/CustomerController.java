package com.devcamp.j28.controller;

import java.util.List;
import java.util.Optional;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;


import com.devcamp.j28.entity.Customer;
import com.devcamp.j28.repository.iCustomerRepository;
import com.devcamp.j28.service.CustomerService;
import com.devcamp.j28.service.ExcelExporter;

@Transactional
@RestController
public class CustomerController {
    @Autowired
    private iCustomerRepository customerRepository;

    @Autowired
    private CustomerService customerService;

    // 1. Get all
    @CrossOrigin
    @GetMapping("/customers")
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    // 2. Get details
    @CrossOrigin
    @GetMapping("/customers/{id}")
    public Customer getCustomerById(@PathVariable int id) {
        if (customerRepository.findById(id).isPresent())
            return customerRepository.findById(id).get();
        else
            return null;
    }

    // 3. Create
    @CrossOrigin
    @PostMapping("/customers")
    public ResponseEntity<Object> createCustomer(@RequestBody Customer customer) {
        try {
            return new ResponseEntity<>(customerService.newCustomer(customer), HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified Customer: "
                    + e.getCause().getCause().getMessage());
        }
    }

    // 4. Update
    @CrossOrigin
    @PutMapping("/customers/{id}")
    public ResponseEntity<Object> updateCustomer(@PathVariable("id") int id, @RequestBody Customer customer) {
        try {
            Optional<Customer> customerData = customerRepository.findById(id);
            if (customerData.isPresent()) {
                Customer newRole = customerData.get();
                newRole.setFirstName(customer.getFirstName());
                newRole.setLastName(customer.getFirstName());
                newRole.setAddress(customer.getAddress());
                newRole.setCity(customer.getCity());
                newRole.setCountry(customer.getCountry());
                newRole.setCreditLimit(customer.getCreditLimit());
                newRole.setPhoneNumber(customer.getPhoneNumber());
                newRole.setPostalCode(customer.getPostalCode());
                newRole.setSalesRepEmployeeNumber(customer.getSalesRepEmployeeNumber());
                newRole.setState(customer.getState());
                newRole.setOrders(customer.getOrders());
                newRole.setPayments(customer.getPayments());
                Customer savedRole = customerRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    // 5. Delete
    @CrossOrigin
    @DeleteMapping("/customers/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable int id) {
        try {
            customerRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @CrossOrigin
    @GetMapping("/export/customers/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);

		List<Customer> customer = new ArrayList<Customer>();

		customerRepository.findAll().forEach(customer::add);

		ExcelExporter excelExporter = new ExcelExporter(customer);

		excelExporter.export(response);
	}

    // 7. Lấy dữ liệu số lượng khách hàng tương ứng với các quốc gia USA, France, Singapore, Spain
    @CrossOrigin
    @GetMapping("/customers2")
    public List<Integer> getAllCustomersByCountry() {
        return customerRepository.countCustomerByCountry();
    }
}
