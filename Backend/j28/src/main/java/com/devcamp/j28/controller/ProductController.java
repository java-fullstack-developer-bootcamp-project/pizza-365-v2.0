package com.devcamp.j28.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import com.devcamp.j28.entity.Product;
import com.devcamp.j28.entity.ProductLine;
import com.devcamp.j28.repository.iProductLineRepository;
import com.devcamp.j28.repository.iProductRepository;

@Transactional
@RestController
public class ProductController {
    @Autowired
    iProductRepository productRepository;

    @Autowired
    private iProductLineRepository productLineRepository;

    // 1. Get all
    @CrossOrigin
    @GetMapping("/products")
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    // 2. Get details
    @CrossOrigin
    @GetMapping("/products/{id}")
    public Product getProductById(@PathVariable int id) {
        if (productRepository.findById(id).isPresent())
            return productRepository.findById(id).get();
        else
            return null;
    }

    // 2.2 Get all payments by customerId
    @CrossOrigin
    @GetMapping("/product-lines/{productLineId}/products")
    public List<Product> getPaymentsByCustomerId(@PathVariable(value = "productLineId") int id) {
        return productRepository.findByProductLineId(id);
    }

    // 3. Create
    @CrossOrigin
    @PostMapping("/product-lines/{productLineId}/products")
    public ResponseEntity<Object> createProduct(@PathVariable("productLineId") int id, @RequestBody Product product) {
        try {
            Optional<ProductLine> productLineData = productLineRepository.findById(id);
            if (productLineData.isPresent()) {
                Product newRole = new Product();
                newRole.setCode(product.getCode());
                newRole.setName(product.getName());
                newRole.setDescription(product.getDescription());
                newRole.setScale(product.getScale());
                newRole.setVendor(product.getVendor());
                newRole.setQuantityInStock(product.getQuantityInStock());
                newRole.setBuyPrice(product.getBuyPrice());
                newRole.setProductLine(product.getProductLine());

                ProductLine _productLine = productLineData.get();
                newRole.setProductLine(_productLine);

                Product savedRole = productRepository.save(newRole);
                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified product: "
                    + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    // 4. Update
    @CrossOrigin
    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable("id") int id, @RequestBody Product product) {
        Optional<Product> productData = productRepository.findById(id);
        if (productData.isPresent()) {
            Product newRole = productData.get();
            newRole.setCode(product.getCode());
            newRole.setName(product.getName());
            newRole.setDescription(product.getDescription());
            newRole.setScale(product.getScale());
            newRole.setVendor(product.getVendor());
            newRole.setQuantityInStock(product.getQuantityInStock());
            newRole.setBuyPrice(product.getBuyPrice());
            //newRole.setProductLine(product.getProductLine());
            Product savedRole = productRepository.save(newRole);
            return new ResponseEntity<>(savedRole, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    // 5. Delete
    @CrossOrigin
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Object> deletepProductById(@PathVariable int id) {
        try {
            productRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
