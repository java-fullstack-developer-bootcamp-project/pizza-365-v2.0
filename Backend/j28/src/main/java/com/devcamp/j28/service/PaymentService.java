package com.devcamp.j28.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.j28.entity.Customer;
import com.devcamp.j28.entity.Payment;
import com.devcamp.j28.repository.iCustomerRepository;
import com.devcamp.j28.repository.iPaymentRepository;

@Service
public class PaymentService {
    @Autowired
    private iPaymentRepository paymentRepository;

    @Autowired
	private iCustomerRepository customerRepository;

    public Payment newPayment(int customerId, Payment payment) {
        Optional<Customer> customerData = customerRepository.findById(customerId);
        if (customerData.isPresent()) {
            Payment newRole = new Payment();
            newRole.setCustomer(payment.getCustomer());
            newRole.setCheckNumber(payment.getCheckNumber());
            newRole.setPaymentDate(payment.getPaymentDate());
            newRole.setAmmount(payment.getAmmount());

            Customer _customer = customerData.get();
            newRole.setCustomer(_customer);

            Payment savedRole = paymentRepository.save(newRole);
            return savedRole;
        }
        else {
            return null;
        }
    }
}
